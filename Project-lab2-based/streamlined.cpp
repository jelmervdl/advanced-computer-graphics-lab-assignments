#include <vector>
#include <iostream>
#include <fstream>
#include <map>
#include <limits>
#include <climits>
#include <cassert>
#include "streamlined.h"

using namespace std;

namespace streamlined {

uint64_t rgb2index(float ir, float ig, float ib)
{
	unsigned int r, g, b;
	r = ir * numeric_limits<unsigned int>::max();
	g = ig * numeric_limits<unsigned int>::max();
	b = ib * numeric_limits<unsigned int>::max();

	uint64_t z = 0;
    for (uint64_t i = 0; i < (sizeof(uint64_t) * CHAR_BIT)/3; ++i) {
        z |= ((r & ((uint64_t) 1 << i)) <<  2 * i)
          |  ((g & ((uint64_t) 1 << i)) << (2 * i + 1))
          |  ((b & ((uint64_t) 1 << i)) << (2 * i + 2));
    }

    return z;
}

Color index2rgb(uint64_t z)
{
	unsigned int r = 0, g = 0, b = 0;
	for (uint64_t i = 0; i < (sizeof(uint64_t) * CHAR_BIT)/3; ++i) {
        r |= ((z & ((uint64_t) 1 << i)) <<  2 * i);
        g |= ((z & ((uint64_t) 1 << i)) << (2 * i + 1));
        b |= ((z & ((uint64_t) 1 << i)) << (2 * i + 2));
    }

    return {
    	(float) r / numeric_limits<unsigned int>::max(),
    	(float) g / numeric_limits<unsigned int>::max(),
    	(float) b / numeric_limits<unsigned int>::max()
    };
}

void write(vector<Surfel> const &pts, ostream &out)
{
	cerr << "Creating table for " << pts.size() << " points" << endl;

	map<uint64_t, vector<size_t> > table;

	for (size_t i = 0, end = pts.size(); i < end; ++i)
	{
		Surfel const &surfel(pts[i]);
		uint64_t table_key = rgb2index(surfel.r, surfel.g, surfel.b);
		table[table_key].push_back(i);
	}

	cerr << "Created table with " << table.size() << " entries" << endl;

	size_t size_buffer = table.size();
	out.write(reinterpret_cast<char const *>(&size_buffer), sizeof(size_buffer));

	cerr << "Written table length" << endl;

	size_t current_offset = 0;
	for (auto itr = table.cbegin(); itr != table.cend(); ++itr)
	{
		out.write(reinterpret_cast<char const *>(&(itr->first)), sizeof(uint64_t));
		out.write(reinterpret_cast<char const *>(&current_offset), sizeof(size_t));
		current_offset += itr->second.size();
		size_buffer = itr->second.size();
		out.write(reinterpret_cast<char const *>(&size_buffer), sizeof(size_t));
	}

	cerr << "Written table" << endl;

	for (auto table_itr = table.cbegin(); table_itr != table.cend(); ++table_itr)
		for (auto pt_itr = table_itr->second.cbegin(), pt_end = table_itr->second.cend(); pt_itr != pt_end; ++pt_itr)
			out.write(reinterpret_cast<char const *>(&(pts[*pt_itr])), sizeof(Surfel));

	cerr << "Written data." << endl;
}

lookup_table read_table(istream &in)
{
	cerr << "Reading table" << endl;
	size_t table_length;
	in.read(reinterpret_cast<char*>(&table_length), sizeof(size_t));

	cerr << "Reading " << table_length << " entries" << endl;
	lookup_table table;

	for (size_t i = 0; i < table_length; ++i)
	{
		uint64_t key;
		size_t offset, length;
		in.read(reinterpret_cast<char*>(&key), sizeof(uint64_t));
		in.read(reinterpret_cast<char*>(&offset), sizeof(size_t));
		in.read(reinterpret_cast<char*>(&length), sizeof(size_t));
		table.push_back({key, offset, length});
	}

	return table;
}

vector<Color> read_indexes(lookup_table const &table)
{
	vector<Color> keys;

	for (auto itr = table.cbegin(); itr != table.cend(); ++itr)
		keys.push_back(index2rgb(itr->key));

	return keys;
}

vector<Surfel> read_points(istream &in, lookup_table const &table, pair<uint64_t, uint64_t> const &range)
{
	size_t pts_size = 0;
	vector<Surfel> pts;

	for (auto itr = table.cbegin(); itr != table.cend(); ++itr)
	{
		if (itr->key < range.first || itr->key > range.second)
			continue;

		pts_size += itr->length;
	}

	pts.reserve(pts_size);

	for (auto itr = table.cbegin(); itr != table.cend(); ++itr)
	{
		if (itr->key < range.first || itr->key > range.second)
			continue;

		in.seekg(itr->offset * sizeof(Surfel));

		for (size_t n = 0; n < itr->length; ++n)
		{
			Surfel surfel;
			in.read(reinterpret_cast<char *>(&surfel), sizeof(Surfel));
			pts.push_back(surfel);
		}
	}

	assert(pts_size == pts.size());

	return pts;
}

vector<Surfel> read_file(string const &filename)
{
	ifstream in(filename);

	lookup_table table(read_table(in));

	for (auto itr = table.cbegin(); itr != table.cend(); ++itr)
		cout << itr->key << ":\t" << itr->length << '\n';

	cout << "Please enter a range:" << endl;

	pair<uint64_t, uint64_t> range;
	cin >> range.first;
	cin >> range.second;

	return read_points(in, table, range);
}

}
