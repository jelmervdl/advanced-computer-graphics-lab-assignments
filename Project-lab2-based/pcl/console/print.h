/*
 * Software License Agreement (BSD License)
 *
 *  Copyright (c) 2010, Willow Garage, Inc.
 *  All rights reserved.
 *
 *  Redistribution and use in source and binary forms, with or without
 *  modification, are permitted provided that the following conditions
 *  are met:
 *
 *   * Redistributions of source code must retain the above copyright
 *     notice, this list of conditions and the following disclaimer.
 *   * Redistributions in binary form must reproduce the above
 *     copyright notice, this list of conditions and the following
 *     disclaimer in the documentation and/or other materials provided
 *     with the distribution.
 *   * Neither the name of the copyright holder(s) nor the names of its
 *     contributors may be used to endorse or promote products derived
 *     from this software without specific prior written permission.
 *
 *  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 *  "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 *  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
 *  FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
 *  COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
 *  INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
 *  BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 *  LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
 *  CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 *  LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
 *  ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 *  POSSIBILITY OF SUCH DAMAGE.
 *
 * $Id$
 *
 */
#ifndef TERMINAL_TOOLS_PRINT_H_
#define TERMINAL_TOOLS_PRINT_H_

#include <stdio.h>
#include <stdarg.h>

#define PCL_ALWAYS(...)  pcl::console::print (pcl::console::L_ALWAYS, __VA_ARGS__)
#define PCL_ERROR(...)   pcl::console::print (pcl::console::L_ERROR, __VA_ARGS__)
#define PCL_WARN(...)    pcl::console::print (pcl::console::L_WARN, __VA_ARGS__)
#define PCL_INFO(...)    pcl::console::print (pcl::console::L_INFO, __VA_ARGS__)
#define PCL_DEBUG(...)   pcl::console::print (pcl::console::L_DEBUG, __VA_ARGS__)
#define PCL_VERBOSE(...) pcl::console::print (pcl::console::L_VERBOSE, __VA_ARGS__)

#define PCL_ASSERT_ERROR_PRINT_CHECK(pred, msg) \
    do \
    { \
        if (!(pred)) \
        { \
            PCL_ERROR(msg); \
            PCL_ERROR("In File %s, in line %d\n" __FILE__, __LINE__); \
        } \
    } while (0)

#define PCL_ASSERT_ERROR_PRINT_RETURN(pred, msg, err) \
    do \
    { \
        PCL_ASSERT_ERROR_PRINT_CHECK(pred, msg); \
        if (!(pred)) return err; \
    } while (0)

namespace pcl
{
  namespace console
  {
    enum VERBOSITY_LEVEL
    {
      L_ALWAYS,
      L_ERROR,
      L_WARN,
      L_INFO,
      L_DEBUG,
      L_VERBOSE
    };

    inline void print (VERBOSITY_LEVEL level, const char *format, ...)
    {
      va_list ap;
      va_start(ap, format);
      vfprintf(stderr, format, ap);
      va_end(ap);
    }
  }
} 

#endif // TERMINAL_TOOLS_PRINT_H_
