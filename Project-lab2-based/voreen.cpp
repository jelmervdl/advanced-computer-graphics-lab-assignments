#include <algorithm>
#include <string>
#include <iostream>
#include <fstream>
#include <sstream>
#include <vector>
#include <cassert>

#include "voreen.h"

using namespace std;

string dirname(string const &filename)
{
	string::const_reverse_iterator pivot(find(filename.rbegin(), filename.rend(), '/'));
	return pivot == filename.rend()
		? filename
		: string(filename.begin(), pivot.base() - 1);
}

string extension(string const &filename)
{
	size_t index = filename.rfind('.');
	return index == std::string::npos
		? string()
		: filename.substr(index + 1);
}

ostream& operator<<(ostream& os, const voreen::VoreenMetaData &metadata)
{
	os << "Filename:   " << metadata.filename << '\n'
	   << "Resolution: " << metadata.resolution[0] << ' ' 
	                     << metadata.resolution[1] << ' ' 
	                     << metadata.resolution[2] << endl;

	return os;
}

voreen::VoreenMetaData voreen::read_metadata(const string &fname)
{
	ifstream ifs(fname);
	size_t lineno = 0;

	voreen::VoreenMetaData metadata;

	for (string line; getline(ifs, line); )
	{
		lineno++;

		// Skip comments
		if (line.empty() || line[0] == '#')
			continue;
			
		// Split line in key -> value pair
		istringstream ss(line);
		string key;

		if (!getline(ss, key, ':')) {
			cerr << "Error in " << fname << " on line " << lineno
			     << ": Could not read key."<< endl;
			exit(1);
		}

		if (key == "ObjectFileName") {
			string filename;
			ss >> filename;
			metadata.filename = dirname(fname) + "/" + filename;
			continue;
		}

		else if (key == "Resolution") {
			ss >> metadata.resolution[0] &&
			ss >> metadata.resolution[1];
			ss >> metadata.resolution[2];

			if (!ss) {
				cerr << "Error in " << fname << " on line " << lineno
				     << ": Could not read resolution."<< endl;
			}
			
			continue;
		}

		else if (key == "Format") {
			string format;
			ss >> format;
			
			if (format == "USHORT")
				metadata.format = VoreenMetaData::USHORT;
			else if (format == "UCHAR")
				metadata.format = VoreenMetaData::UCHAR;
			else
				cerr << "Error in " << fname << " on line " << lineno
				     << ": Unsupported format."<< endl;

			continue;
		}

		else {
			cerr << "Ignoring key " << key
			     << " in " << fname << " on line " << lineno << endl;
		}
	}

	return metadata;
}

vector<Surfel> voreen::read_points(voreen::VoreenMetaData const &metadata)
{
	ifstream ifs(metadata.filename);

	if (!ifs) {
		cerr << "Error: Could not open " << metadata.filename << " for reading." << endl;
		exit(2);
	}

	size_t n_pts = metadata.resolution[0] * metadata.resolution[1] * metadata.resolution[2];
	vector<Surfel> points(n_pts);

	float max_color = 0.0f;
	size_t max_res = max(max(metadata.resolution[0], metadata.resolution[1]), metadata.resolution[2]);

	for (size_t i = 0; i < n_pts; ++i)
	{
		Surfel &surf(points[i]);

		surf.x = i % metadata.resolution[0];
		surf.y = (i / metadata.resolution[0]) % metadata.resolution[1];
		surf.z = (i / metadata.resolution[0]) / metadata.resolution[1];

		surf.uvec[0] = 0.1;
		surf.uvec[1] = 0.1;
		surf.uvec[2] = 0.1;

		surf.vvec[0] = 0.1;
		surf.vvec[1] = 0.1;
		surf.vvec[2] = 0.1;

		surf.x /= max_res;
		surf.y /= max_res;
		surf.z /= max_res;
		
		switch (metadata.format)
		{
			case VoreenMetaData::USHORT:
				{
					unsigned short color;
					ifs.read(reinterpret_cast<char*>(&color), sizeof(unsigned short));
					surf.color[0] = color;
				}
				break;

			case VoreenMetaData::UCHAR:
				{
					unsigned char color;
					ifs.read(reinterpret_cast<char*>(&color), sizeof(unsigned char));
					surf.color[0] = color;
				}
				break;
		}

		if (surf.color[0] > max_color)
			max_color = surf.color[0];
	}

	// Normalize color
	for (auto itr = points.begin(), end = points.end(); itr < end; ++itr) {
		float normalized = itr->color[0] / max_color;
		assert(normalized <= 1.0f);
		
		itr->r = normalized;
		itr->g = normalized;
		itr->b = normalized;
	}

	return points;
}

vector<Surfel> voreen::read_file(const string &fname)
{
	voreen::VoreenMetaData metadata(voreen::read_metadata(fname));
	cerr << metadata << endl;

	return voreen::read_points(metadata);
}
