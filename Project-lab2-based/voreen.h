#ifndef VOREEN_H
#define VOREEN_H

#include "surfel.h"
#include <string>
#include <vector>

namespace voreen {

struct VoreenMetaData
{
	enum Format { UCHAR, USHORT };

	size_t resolution[3];
	std::string filename;
	Format format;
};

VoreenMetaData read_metadata(const std::string &fname);

std::vector<Surfel> read_points(VoreenMetaData const &metadata);

std::vector<Surfel> read_file(const std::string &fname);

}

#endif
