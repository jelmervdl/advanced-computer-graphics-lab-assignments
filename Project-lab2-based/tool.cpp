#include <fstream>
#include <vector>
#include <Eigen/Dense>
#include <pcl/common/eigen.h>
#include "voreen.h"
#include "streamlined.h"

using namespace std;

int usage(char const *program)
{
	cerr << "Usage: " << program << " <mode> <file>\n"
		"\n"
		"Mode can be:\n"
		"  show: print lookup table\n"
		"  convert: convert voreen formatted data to streamlined data\n"
		<< endl;

	return -1;
}

int run_show(const char *input)
{
	streamlined::lookup_table table;

	if (input == NULL)
		table = streamlined::read_table(cin);
	else {
		ifstream fin(input, std::ifstream::in | std::ifstream::binary);
		table = streamlined::read_table(fin);
		fin.close();
	}

	for (auto itr = table.cbegin(); itr != table.cend(); ++itr)
		cout << itr->key << ":\t" << itr->offset << "\t" << itr->length << endl;

	return 0;
}

// Adapted from https://github.com/PointCloudLibrary/pcl/blob/647de7bed7df7cd383e5948ff42b116e5aae0e79/common/include/pcl/common/impl/centroid.hpp#L489
void compute_mean_and_covariance_matrix(vector<Surfel> const &cloud,
	Eigen::Matrix<float, 3, 3> &covariance_matrix,
	Eigen::Matrix<float, 4, 1> &centroid)
{
	Eigen::Matrix<float, 1, 9, Eigen::RowMajor> accu = Eigen::Matrix<float, 1, 9, Eigen::RowMajor>::Zero();
	
	for (size_t i = 0; i < cloud.size(); ++i) {
		accu[0] += cloud[i].x * cloud[i].x;
		accu[1] += cloud[i].x * cloud[i].y;
		accu[2] += cloud[i].x * cloud[i].z;
		accu[3] += cloud[i].y * cloud[i].y; // 4
		accu[4] += cloud[i].y * cloud[i].z; // 5
		accu[5] += cloud[i].z * cloud[i].z; // 8
		accu[6] += cloud[i].x;
		accu[7] += cloud[i].y;
		accu[8] += cloud[i].z;
	}
	
	accu /= static_cast<float>(cloud.size());
	
	centroid[0] = accu[6];
	centroid[1] = accu[7];
	centroid[2] = accu[8];
	centroid[3] = 1;

	covariance_matrix.coeffRef(0) = accu[0] - accu[6] * accu[6];
	covariance_matrix.coeffRef(1) = accu[1] - accu[6] * accu[7];
	covariance_matrix.coeffRef(2) = accu[2] - accu[6] * accu[8];
	covariance_matrix.coeffRef(4) = accu[3] - accu[7] * accu[7];
	covariance_matrix.coeffRef(5) = accu[4] - accu[7] * accu[8];
	covariance_matrix.coeffRef(8) = accu[5] - accu[8] * accu[8];
	covariance_matrix.coeffRef(3) = covariance_matrix.coeff(1);
	covariance_matrix.coeffRef(6) = covariance_matrix.coeff(2);
	covariance_matrix.coeffRef(7) = covariance_matrix.coeff(5);
}

void solve_plane_parameters(const Eigen::Matrix3f &covariance_matrix,
	const Eigen::Vector4f &point, float &nx, float &ny, float &nz, float &curvature)
{
	// Extract the smallest eigenvalue and its eigenvector
	EIGEN_ALIGN16 Eigen::Vector3f::Scalar eigen_value;
	EIGEN_ALIGN16 Eigen::Vector3f eigen_vector;
	pcl::eigen33(covariance_matrix, eigen_value, eigen_vector);

	nx = eigen_vector[0];
	ny = eigen_vector[1];
	nz = eigen_vector[2];

	// Compute the curvature surface change
	float eig_sum = covariance_matrix.coeff(0) + covariance_matrix.coeff(4) + covariance_matrix.coeff(8);
	curvature = eig_sum == 0 ? 0 : fabsf(eigen_value / eig_sum);
}

// Adapted from:
// https://github.com/PointCloudLibrary/pcl/blob/master/features/include/pcl/features/normal_3d.h#L60
// https://github.com/PointCloudLibrary/pcl/blob/647de7bed7df7cd383e5948ff42b116e5aae0e79/features/include/pcl/features/impl/feature.hpp#L48
void estimate_normals(vector<Surfel> &surfels)
{
	for (auto itr = surfels.begin(); itr != surfels.end(); ++itr)
	{
		// Placeholder for the 3x3 covariance matrix at each surface patch
		EIGEN_ALIGN16 Eigen::Matrix3f covariance_matrix;
		// 16-bytes aligned placeholder for the XYZ centroid of a surface patch
		Eigen::Vector4f xyz_centroid;

		float curvature;
		
		// Todo: Select only a few surfels near itr. Could be solved using a tree or something similar
		vector<Surfel> area_surfels = surfels;

		compute_mean_and_covariance_matrix(area_surfels, covariance_matrix, xyz_centroid);

		solve_plane_parameters(covariance_matrix, xyz_centroid, itr->nx, itr->ny, itr->nz, curvature);
	}		
}

int run_convert(const char *input, const char *output)
{
	cerr << "Reading " << input << endl;
	vector<Surfel> surfels = voreen::read_file(input);

	// cerr << "Estimating normals of " << surfels.size() << " surfels" << endl;
	// estimate_normals(surfels);
	
	cerr << "Writing output" << endl;
	if (output == NULL) {
		streamlined::write(surfels, cout);
	}
	else {
		ofstream fout(output, std::ofstream::out | std::ofstream::binary);
		streamlined::write(surfels, fout);
		fout.close();
	}

	return 0;
}

int main(int argc, char *argv[])
{
	if (argc < 3 || argc > 4)
		return usage(argv[0]);

	string mode(argv[1]);

	if (mode == "show")
		return run_show(argv[2]);
	else if (mode == "convert")
		return run_convert(argv[2], argc > 3 ? argv[3] : NULL);
	else
		return usage(argv[1]);
}
