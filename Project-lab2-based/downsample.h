#ifndef DOWNSAMPLE_H
#define DOWNSAMPLE_H

#include <vector>
#include "surfel.h"

std::vector<Surfel> downsample(std::vector<Surfel> const &origin, size_t target_size);

#endif
