#ifndef STREAMLINED_H
#define STREAMLINED_H

#include <stdint.h>
#include <vector>
#include <iostream>
#include <map>
#include "surfel.h"

namespace streamlined {

struct table_entry
{
	uint64_t key;
	size_t offset;
	size_t length;
};

typedef std::vector<table_entry> lookup_table;

void write(std::vector<Surfel> const &pts, std::ostream &out);

lookup_table read_table(std::istream &in);

std::vector<Color> read_indexes(lookup_table const &table);

std::vector<Surfel> read_points(std::istream &in, lookup_table const &table, std::pair<uint64_t, uint64_t> const &range);

std::vector<Surfel> read_file(std::string const &filename);

}

#endif
