#ifndef SURFEL_H
#define SURFEL_H

struct Color {
	float r, g, b;
};

struct Surfel
{
	union {
		float pos[3];
		struct { float x, y, z; };
	};

	union {
		float color[3];
		struct {float r, g, b; };
	};

	union {
		float normal[3];
		struct {float nx, ny, nz; };
	};

	float uvec[3];
	float vvec[3];
};

#endif
