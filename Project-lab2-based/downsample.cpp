#include <stddef.h>
#include <cstdlib>
#include <vector>
#include "surfel.h"
#include "downsample.h"

std::vector<Surfel> downsample(std::vector<Surfel> const &origin, size_t target_size)
{
     std::vector<Surfel> out(target_size);
     size_t step_size = origin.size() / target_size;

     for (size_t i = 0; i < target_size; ++i) {
     	size_t offset = (double) rand() / RAND_MAX * step_size;
        out[i] = origin[i * step_size + offset];
     }

     return out;
}

