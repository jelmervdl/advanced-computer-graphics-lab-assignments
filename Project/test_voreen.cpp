#include <iostream>
#include "voreen.h"

using namespace std;

int main(int argc, char *argv[])
{
	if (argc < 2) {
		cerr << "Usage: " << argv[0] << " file" << endl;
		return 1;
	}

	vector<Surfel> data = voreen_read_file(argv[1]);

	cout << "Points: " << endl;
	
	for (auto itr = data.begin(); itr != data.end(); ++itr) {
		cout << '{'
			<< '{' << itr->x << ',' << itr->y << ',' << itr->z << '}'
			<< ','
			<< '{' << itr->r << ',' << itr->g << ',' << itr->b << '}'
		     << '}' << endl;
	}

	return 0;
}
