#ifndef VOREEN_H
#define VOREEN_H

#include "surfel.h"
#include <string>
#include <vector>

struct VoreenMetaData
{
	enum Format { UCHAR, USHORT };

	size_t resolution[3];
	std::string filename;
	Format format;
};

VoreenMetaData voreen_read_metadata(const std::string &fname);

std::vector<Surfel> voreen_read_points(VoreenMetaData const &metadata);

std::vector<Surfel> voreen_read_file(const std::string &fname);

#endif
