#ifndef SURFEL_H
#define SURFEL_H

struct Surfel
{
    union {
    	float pos[3];
    	struct { float x, y, z; };
   	};

    union {
    	float color[3];
    	struct {float r, g, b; };
    };

    float uvec[3];
    float vvec[3];
};

#endif
